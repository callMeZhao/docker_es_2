import matplotlib.pyplot as plt

def drawgraph(x, y, color, txt, plot, figure):
	plot.plot(x, y, color,label = txt)
	plot.title("Docker Experiment Linux PC")
	plot.ylabel("Available Memory(Mb)")
	plot.ylim([1500, None])
	plot.xlabel("number of concurrent running sieve")
	plot.legend()

	pass

figure = plt.figure()
container = [0, 1, 2, 4, 8]
procs = [0, 1, 2, 4, 8, 16, 32, 50, 60]
# mem = [3071, 3022, 2979, 2920, 2722]
mem = [2343, 2339, 2338, 2337, 2336, 2335, 2333, 2332, 2335]

drawgraph(procs, mem, 'r*', 'Concurrent Processes', plt, figure)
plt.show()
# figure.savefig('plot.png')