# __author__ = 'ZIZHAO'
import matplotlib.pyplot as plt

def drawgraph(x, y, color, txt, plot, figure):
	plot.plot(x, y, color,label = txt)
	plot.title("Docker Experiment PC")
	plot.ylabel("performance(seconds)")
	plot.ylim([0, None])
	plot.xlabel("number of Sieve apps")
	plot.legend()

	pass

n_list_one = [1, 2, 4, 8, 16, 32]
n_list_two = [2, 4, 8, 16, 32, 36]
n_list_four = [4, 8, 16, 32]
n_list_eight = [8, 16, 32]

figure = plt.figure()

time_plot = list()
# open file and read the content in a list
with open('docker_paral_one_sieve100.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot.append(float(currentPlace))

time_plot2 = list()
# open file and read the content in a list
with open('docker_paral_two_sieve100.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot2.append(float(currentPlace))

# define an empty list
time_plot3 = list()
# open file and read the content in a list
with open('docker_paral_four_sieve100.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot3.append(float(currentPlace))

# define an empty list
time_plot4 = list()
# open file and read the content in a list
with open('docker_paral_eight_sieve100.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot4.append(float(currentPlace))

drawgraph(n_list_one, time_plot2, 'y*', 'two sieve one container', plt, figure)
drawgraph(n_list_two, time_plot, 'g*', 'one sieve one container', plt, figure)
drawgraph(n_list_four, time_plot3, 'b*', 'four sieve one container', plt, figure)
drawgraph(n_list_eight, time_plot4, 'r*', 'eight sieve one container', plt, figure)

plt.show()
print(time_plot, time_plot2, time_plot3, time_plot4)
figure.savefig('plot.png')