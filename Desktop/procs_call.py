import subprocess
from subprocess import Popen

def sequ_call(n, command):
	if n == 1:
		subprocess.call(command.split())
	else:	
		for x in range(0, n): # it runs (n - 0) times
			subprocess.call(command.split())

def paral_call(n, command):
	commandList = [command] * n
	procs = [Popen(i.split()) for i in commandList]
	for p in procs:
		p.wait()