import sys
import os
import time
from procs_call import paral_call, sequ_call
from watchdog import Watchdog

def seq_command_exe_n(command, n_list, time_list):
	for n in n_list:
		start = time.time()
		sequ_call(n, command)
		end = time.time()
		time_list.append(end - start)
		print('sequ_call', command, n)
	pass

def paral_command_exe_n(command, n_list, time_list):
	for n in n_list:
		start = time.time()
		paral_call(n, command)
		end = time.time()
		time_list.append(end - start)
		print('paral_call', command, n)
	pass

def myHandler():
	print('Whoa! watchdog expired')
	os.system('docker container stop $(docker container ls -aq)')
	exit()



#n_list = [1, 2, 4, 8]
# n_list =  [60] # memory problem occurs
#n_list = [16, 18, 20, 22, 24] #single sieve in each container

n_list = [1, 2, 4, 8, 16, 32, 50, 60] #one sieve in each container
# n_list = [1, 2, 4, 8, 16, 18] #two sieves in each container
# n_list = [1, 2, 4, 8] #four sieves in each container
# n_list = [1, 2, 4] #eight sieves in each container

# n_list = [20, 22, 24]

# command = "./sieve"
# time_list = list()
# seq_command_exe_n(command, n_list, time_list)

# with open('seq_latency.txt', 'w') as filehandle:
#     for time in time_list:
#         filehandle.write('%s\n' % time)


# command = "docker run sieve"
# docker_time_list = list()
# seq_command_exe_n(command, n_list, docker_time_list)

# with open('docker_seq_latency.txt', 'w') as filehandle:
#     for time in docker_time_list:
#         filehandle.write('%s\n' % time)


# command = "./sieve"
# para_time_list = list()
# paral_command_exe_n(command, n_list, para_time_list)

# with open('paral_latency.txt', 'w') as filehandle:
#     for time in para_time_list:
#         filehandle.write('%s\n' % time)


command = "docker run sieve_one"
# command = "docker run sieve_eight"
# command = "./sieve.out"
watchdog = Watchdog(20, myHandler)
docekr_paral_time_list = list()
paral_command_exe_n(command, n_list, docekr_paral_time_list)
watchdog.reset()
# with open('paral_one_sieve5000.txt', 'w') as filehandle:
# # with open('docker_paral_eight_sieve.txt', 'w') as filehandle:
#    for time in docekr_paral_time_list:
#        filehandle.write('%s\n' % time)

#
# ##### read text script
# # define an empty list
# time_plot = []
#
# # open file and read the content in a list
# with open('seq_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'seq time plot', time_plot)
#
# # define an empty list
# time_plot2 = []
#
# # open file and read the content in a list
# with open('docker_seq_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'seq docker time plot', time_plot)
#
# # define an empty list
# time_plot3 = []
# # open file and read the content in a list
# with open('paral_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'paral time plot', time_plot)
#
# # define an empty list
# time_plot4 = []
# # open file and read the content in a list
# with open('docker_paral_latency.txt', 'r') as filehandle:
#     for line in filehandle:
#         # remove linebreak which is the last character of the string
#         currentPlace = line[:-1]
#
#         # add item to the list
#         time_plot.append(currentPlace)
#
# print( 'paral docker time plot', time_plot)
#
