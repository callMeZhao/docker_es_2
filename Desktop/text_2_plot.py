# __author__ = 'ZIZHAO'
import matplotlib.pyplot as plt

def drawgraph(x, y, color, txt, plot, figure):
	plot.plot(x, y, color,label = txt)
	plot.title("Docker Experiment Pi")
	plot.ylabel("performance(seconds)")
	plot.ylim([0, None])
	plot.xlabel("number of Sieve apps")
	plot.legend()

	pass

n_list = [1, 2, 4, 8]
figure = plt.figure()

time_plot = list()
# open file and read the content in a list
with open('seq_latency.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot.append(float(currentPlace))

time_plot2 = list()
# open file and read the content in a list
with open('docker_seq_latency.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot2.append(float(currentPlace))

# define an empty list
time_plot3 = list()
# open file and read the content in a list
with open('paral_latency.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot3.append(float(currentPlace))

# define an empty list
time_plot4 = list()
# open file and read the content in a list
with open('docker_paral_latency.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot4.append(float(currentPlace))

drawgraph(n_list, time_plot2, 'y^', 'sequ with Docker', plt, figure)
drawgraph(n_list, time_plot, 'g^', 'sequ without Docker', plt, figure)
drawgraph(n_list, time_plot3, 'b^', 'paral without Docker', plt, figure)
drawgraph(n_list, time_plot4, 'r^', 'paral with Docker', plt, figure)

plt.show()
print(time_plot, time_plot2, time_plot3, time_plot4)
figure.savefig('plot.png')