# __author__ = 'ZIZHAO'
import matplotlib.pyplot as plt

def drawgraph(x, y, color, txt, plot, figure):
    plot.plot(x, y, color,label = txt)
    plot.title("Memory Usage with Scalling-up contianers and fixed #sieve")
    plot.ylabel("Memory Usage")
    plot.ylim([200, None])
    plot.xlabel("Number of Containers")
    plot.xlim([0, 24])
    plot.legend()

    pass

# n_list = [0, 1, 2, 4, 8, 16, 24, 32, 40]

n_list = [0, 1, 3, 4, 6, 8, 12, 24]

figure = plt.figure()

time_plot = list()
with open('memory_usage.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot.append(float(currentPlace))

# time_plot2 = list()
# with open('free_swap.txt', 'r') as filehandle:
#     for line in filehandle:
#         currentPlace = line[:-1]
#         time_plot2.append(float(currentPlace))

drawgraph(n_list, time_plot, 'gs-', 'memory usage with 200MB as base', plt, figure)
# drawgraph(n_list, time_plot2, 'b8-', 'free swap', plt, figure)



plt.show()
print(time_plot, time_plot2, time_plot3, time_plot4, time_plot5, time_plot6)
figure.savefig('plot.png')