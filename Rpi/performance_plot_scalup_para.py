# __author__ = 'ZIZHAO'
import matplotlib.pyplot as plt

def drawgraph(x, y, color, txt, plot, figure):
    plot.plot(x, y, color,label = txt)
    plot.title("Latency with Scaling-up Sieve Parameters")
    plot.ylabel("Latency(seconds)")
    plot.ylim([0, None])
    plot.xlabel("Sieve Parameters")
    plot.xlim([1000, None])
    plot.legend()

    pass

n_list = [1000, 5000, 10000]

figure = plt.figure()

time_plot = list()
with open('8_sieve_3_container.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot.append(float(currentPlace))

time_plot2 = list()
with open('4_sieve_6_container.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot2.append(float(currentPlace))

time_plot3 = list()
with open('2_sieve_12_container.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot3.append(float(currentPlace))

time_plot4 = list()
with open('1_sieve_24_container.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot4.append(float(currentPlace))

time_plot5 = list()
with open('24_sieve_1_container.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot5.append(float(currentPlace))

time_plot6 = list()
with open('24_sieve_0_container.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot6.append(float(currentPlace))

time_plot7 = list()
with open('6_sieve_4_container.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot7.append(float(currentPlace))


drawgraph(n_list, time_plot4, 'r*-', '24_container_1_sieve_each', plt, figure)
drawgraph(n_list, time_plot3, 'bo-', '12_container_2_sieve_each', plt, figure)
drawgraph(n_list, time_plot2, 'y8-', '6_container_4_sieve_each', plt, figure)
drawgraph(n_list, time_plot7, 'c8-', '4_container_6_sieve_each', plt, figure)
drawgraph(n_list, time_plot, 'gs-', '3_container_8_sieve_each', plt, figure)
drawgraph(n_list, time_plot5, 'k^-', '1_container_24_sieve_each', plt, figure)
drawgraph(n_list, time_plot6, 'mD-', '0_container_24_sieve', plt, figure)


plt.show()
print(time_plot, time_plot2, time_plot3, time_plot4, time_plot5, time_plot6)
figure.savefig('plot.png')