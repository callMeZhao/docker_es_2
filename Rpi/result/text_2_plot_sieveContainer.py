# __author__ = 'ZIZHAO'
import matplotlib.pyplot as plt

def drawgraph(x, y, color, txt, plot, figure):
	plot.plot(x, y, color,label = txt)
	plot.title("Latency and scaling-up Sieves when the Parameter is 5000")
	plot.ylabel("Latency(seconds)")
	plot.ylim([0, None])
	plot.xlabel("Number of Containers")
	plot.legend()

	pass

n_list_one = [1, 2, 4, 8, 16, 24]
n_list_two = [2, 4, 8, 16, 32, 48]
n_list_four = [4, 8, 16, 32, 64, 96]
n_list_eight = [8, 16, 32, 64, 128, 192]

figure = plt.figure()

time_plot = list()
# open file and read the content in a list
with open('sieve_pi_5000_one_12481624.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot.append(float(currentPlace))

time_plot2 = list()
# open file and read the content in a list
with open('sieve_pi_5000_two_12481624.txt', 'r') as filehandle:
    for line in filehandle:
        currentPlace = line[:-1]
        time_plot2.append(float(currentPlace))

# define an empty list
time_plot3 = list()
# open file and read the content in a list
with open('sieve_pi_5000_four_12481624.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot3.append(float(currentPlace))

# define an empty list
time_plot4 = list()
# open file and read the content in a list
with open('sieve_pi_5000_eight_12481624.txt', 'r') as filehandle:
    for line in filehandle:
        # remove linebreak which is the last character of the string
        currentPlace = line[:-1]
        time_plot4.append(float(currentPlace))

drawgraph(n_list_eight, time_plot4, 'r*-', 'eight/container 5000', plt, figure)
drawgraph(n_list_four, time_plot3, 'bo-', 'four/container 5000', plt, figure)
drawgraph(n_list_two, time_plot2, 'y8-', 'two/container 5000', plt, figure)
drawgraph(n_list_one, time_plot, 'gs-', 'one/container 5000', plt, figure)


plt.show()
print(time_plot, time_plot2, time_plot3, time_plot4)
figure.savefig('plot.png')