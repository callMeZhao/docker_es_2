import sys
import os
import time
from procs_call import paral_call, sequ_call
from watchdog import Watchdog

def paral_command_exe_n(command_list, n, time_list):
	for command in command_list:
		print('paral_call', command, n)
		start = time.time()
		paral_call(n, command)
		end = time.time()
		time_list.append(end - start)
		# watchdog.reset()
		print('paral_call done')
	pass

# def myHandler():
# 	print('Whoa! watchdog expired')
# 	sys.exit()
# 	os.system('docker container stop $(docker container ls -aq)')
# 	sys.exit()

n = 4 
command_list =["docker run sieve_1000_6s", "docker run sieve_5000_6s", "docker run sieve_10000_6s"]
#command_list = ["./sieve_pi_5000.out"]
#command_list = ["docker run sieve_5000_1s"]
docekr_paral_time_list = list()
paral_command_exe_n(command_list, n, docekr_paral_time_list)
with open('6_sieve_4_container.txt', 'w') as filehandle: #only use parallel processing in this testing
   for time in docekr_paral_time_list:
       filehandle.write('%s\n' % time)

# command = "docker run sieve_5000_24s"
# docekr_paral_time_list = list()
# paral_command_exe_n(command, n_list, docekr_paral_time_list)
# with open('24_sieve_1_container.txt', 'a') as filehandle: #only use parallel processing in this testing
#    for time in docekr_paral_time_list:
#        filehandle.write('%s\n' % time)

# command = "docker run sieve_10000_24s"
# docekr_paral_time_list = list()
# paral_command_exe_n(command, n_list, docekr_paral_time_list)
# with open('24_sieve_1_container.txt', 'a') as filehandle: #only use parallel processing in this testing
#    for time in docekr_paral_time_list:
#        filehandle.write('%s\n' % time)
