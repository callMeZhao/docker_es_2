import sys
import os
import time
from procs_call import paral_call, sequ_call
from watchdog import Watchdog

def paral_command_exe_n(command, n_list, time_list, watchdog):
	for n in n_list:
		print('paral_call', command, n)
		start = time.time()
		paral_call(n, command)
		end = time.time()
		time_list.append(end - start)
		watchdog.reset()
		print('paral_call done')
	pass

def myHandler():
	print('Whoa! watchdog expired')
	sys.exit()
	os.system('docker container stop $(docker container ls -aq)')
	sys.exit()

n_list = [1, 2, 4, 8, 16, 24, 32, 40]
#n_list = [40, 48, 56]
command = "docker run sieve_5000_4s" # four cores all work
watchdog = Watchdog(1500, myHandler)
docekr_paral_time_list = list()
paral_command_exe_n(command, n_list, docekr_paral_time_list, watchdog)

watchdog.stop()
with open('docker_pi_1_40.txt', 'w') as filehandle: #only use parallel processing in this testing
   for time in docekr_paral_time_list:
       filehandle.write('%s\n' % time)

